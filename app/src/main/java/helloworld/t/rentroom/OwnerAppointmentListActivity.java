package helloworld.t.rentroom;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import helloworld.t.rentroom.Common.Common;
import helloworld.t.rentroom.Model.Appointment;
import helloworld.t.rentroom.ViewHolder.AppointmentViewHolder;
import helloworld.t.rentroom.ViewHolder.OwnerAppointmentViewHolder;

public class OwnerAppointmentListActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseRecyclerAdapter<Appointment, OwnerAppointmentViewHolder> adapter;


    FirebaseDatabase database;
    DatabaseReference appointments;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_appointment_list);


        database = FirebaseDatabase.getInstance();
        appointments = database.getReference("Appointments");

        recyclerView = findViewById(R.id.listAppointments);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        loadAppointments(Common.currentUser.getUsername());
    }

    private void loadAppointments(String username) {

        Query getAppointmentByUser=appointments.orderByChild("ownerUsername").equalTo(username);

        FirebaseRecyclerOptions<Appointment> orderOptions=new FirebaseRecyclerOptions.Builder<Appointment>()
                .setQuery(getAppointmentByUser,Appointment.class)
                .build();

        adapter=new FirebaseRecyclerAdapter<Appointment, OwnerAppointmentViewHolder>(orderOptions) {

            @Override
            protected void onBindViewHolder(@NonNull OwnerAppointmentViewHolder holder, final int position, @NonNull final Appointment model) {

                holder.txtAppointmentId.setText("Id : "+adapter.getRef(position).getKey());
                holder.txtMemberUsername.setText("Member Username : "+model.getMemberUsername());
                holder.txtRoomId.setText("Room Id : "+model.getRoomId());
                holder.txtStatus.setText("Status : "+model.getStatus());
                holder.txtDateTime.setText("Date/Time : "+model.getDateTime());
                holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(model.getStatus().equals("Deal")){
                            model.setStatus("Requesting");
                            appointments.child(adapter.getRef(position).getKey()).setValue(model);
                            Toast.makeText(OwnerAppointmentListActivity.this,"Status Updated",Toast.LENGTH_SHORT).show();
                        }else{

                            Toast.makeText(OwnerAppointmentListActivity.this,"Status Not in Deal",Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                holder.btn_deal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(model.getStatus().equals("Requesting")){
                            model.setStatus("Deal");
                            appointments.child(adapter.getRef(position).getKey()).setValue(model);
                            Toast.makeText(OwnerAppointmentListActivity.this,"Status Updated",Toast.LENGTH_SHORT).show();
                        }else if(model.getStatus().equals("Deal")){
                            model.setStatus("Completed");
                            appointments.child(adapter.getRef(position).getKey()).setValue(model);
                            Toast.makeText(OwnerAppointmentListActivity.this,"Status Updated",Toast.LENGTH_SHORT).show();
                        }
                        else{

                            Toast.makeText(OwnerAppointmentListActivity.this,"Status Not in Requesting/Deal",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                holder.btn_chat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent chat=new Intent(OwnerAppointmentListActivity.this,ChatActivity.class);
                        chat.putExtra(Common.appointmentId,adapter.getRef(position).getKey());
                        startActivity(chat);
                    }
                });
            }


            @Override
            public OwnerAppointmentViewHolder onCreateViewHolder(ViewGroup parent, int i) {
                View itemView= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.owner_appointment_item,parent,false);
                return new OwnerAppointmentViewHolder(itemView);
            }
        };
        adapter.startListening();
        recyclerView.setAdapter(adapter);

    }
}

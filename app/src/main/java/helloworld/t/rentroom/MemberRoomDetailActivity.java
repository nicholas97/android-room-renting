package helloworld.t.rentroom;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

import helloworld.t.rentroom.Common.Common;
import helloworld.t.rentroom.Model.Appointment;
import helloworld.t.rentroom.Model.Rating;
import helloworld.t.rentroom.Model.Room;

public class MemberRoomDetailActivity extends AppCompatActivity implements RatingDialogListener {

    TextView room_name,room_price,room_description;
    ImageView room_image;
    CollapsingToolbarLayout collapsingToolbarLayout;
    RatingBar ratingBar;

    FirebaseDatabase database;
    DatabaseReference rooms;
    DatabaseReference ratingTbl;
    DatabaseReference appointments;

    FloatingActionButton btnAppointment,btnRating;
    Button btnShowComment;

    String roomId = "";

    Room currentRoom;

    EditText edtDateTime;
    boolean dateTimeSelected;
    String dateTimeSelectedString="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_room_detail);


        database = FirebaseDatabase.getInstance();
        rooms = database.getReference("Rooms");
        ratingTbl=database.getReference("Rating");
        appointments=database.getReference("Appointments");

        ratingBar=(RatingBar)findViewById(R.id.ratingBar);
        room_description = (TextView) findViewById(R.id.room_description);
        room_name = (TextView) findViewById(R.id.room_name);
        room_price = (TextView) findViewById(R.id.room_price);
        room_image = (ImageView) findViewById(R.id.img_room);

        btnRating=(FloatingActionButton)findViewById(R.id.btnRating);
        btnRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRatingDoalog();
            }
        });

        btnAppointment=(FloatingActionButton)findViewById(R.id.btnAppointment);
        btnAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddAppointmentDialog();
            }
        });

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppbar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppbar);



        btnShowComment=(Button)findViewById(R.id.btnShowComment);
        btnShowComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MemberRoomDetailActivity.this,ShowCommentActivity.class);
                intent.putExtra(Common.roomId,roomId);
                startActivity(intent);


            }
        });

        if(getIntent() != null) {
            roomId = getIntent().getStringExtra(Common.roomId);
        }
        if(!roomId.isEmpty()){
            if(Common.isConnectedToInternet(getBaseContext())) {
                getDetailRoom(roomId);
                getRatingRoom(roomId);
            }
            else{
                Toast.makeText(MemberRoomDetailActivity.this,"Please check your connection!",Toast.LENGTH_SHORT).show();
                return;


            }
        }


    }

    private void showAddAppointmentDialog() {

        dateTimeSelected=false;
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(MemberRoomDetailActivity.this);
        alertDialog.setTitle("Add new Appointment");
        alertDialog.setMessage("Please Select Date/Time");

        LayoutInflater inflater=this.getLayoutInflater();
        View add_appointment_layout=inflater.inflate(R.layout.add_appointment,null);

        edtDateTime=add_appointment_layout.findViewById(R.id.edtDateTime);


        edtDateTime.setInputType(InputType.TYPE_NULL);

        edtDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateTimeDialog(edtDateTime);
            }
        });




        alertDialog.setView(add_appointment_layout);
        alertDialog.setIcon(R.drawable.ic_appointment);

        //Set Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Create new appointment
                if(dateTimeSelected){
                    dialogInterface.dismiss();

                    Appointment newAppointment= new Appointment();
                    newAppointment.setMemberUsername(Common.currentUser.getUsername());
                    newAppointment.setOwnerUsername(currentRoom.getOwnerUsername());
                    newAppointment.setRoomId(roomId);
                    newAppointment.setDateTime(dateTimeSelectedString);
                    newAppointment.setStatus("Requesting");
                    appointments.push().setValue(newAppointment);
                    Toast.makeText(MemberRoomDetailActivity.this,"New Appointment was Added",Toast.LENGTH_SHORT).show();

                }else{
                    Toast.makeText(MemberRoomDetailActivity.this,"Please Select Date/Time",Toast.LENGTH_SHORT).show();

                }

            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }
    private void showDateTimeDialog(final EditText date_time_in) {
        final Calendar calendar=Calendar.getInstance();
        DatePickerDialog.OnDateSetListener dateSetListener=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);

                TimePickerDialog.OnTimeSetListener timeSetListener=new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        calendar.set(Calendar.MINUTE,minute);

                        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yy-MM-dd HH:mm");

                        dateTimeSelected=true;
                        dateTimeSelectedString=simpleDateFormat.format(calendar.getTime());
                        date_time_in.setText(dateTimeSelectedString);

                    }
                };

                new TimePickerDialog(MemberRoomDetailActivity.this,timeSetListener,calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE),false).show();
            }
        };

        new DatePickerDialog(MemberRoomDetailActivity.this,dateSetListener,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();

    }

    private void showRatingDoalog() {
        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                .setNoteDescriptions(Arrays.asList("Very Bad","Not Good","Quite Ok","Vey Good","Excellent"))
                .setDefaultRating(1)
                .setTitle("Rate this Room")
                .setDescription("Please rating to help us improve our services")
                .setTitleTextColor(R.color.colorPrimary)
                .setDescriptionTextColor(R.color.colorPrimary)
                .setHint("Your Comment ....")
                .setHintTextColor(R.color.colorAccent)
                .setCommentTextColor(android.R.color.white)
                .setCommentBackgroundColor(R.color.colorPrimaryDark)
                .setWindowAnimation(R.style.RatingDialogFadeAnim)
                .create(MemberRoomDetailActivity.this)
                .show();
    }

    @Override
    public void onNegativeButtonClicked() {

    }

    @Override
    public void onPositiveButtonClicked(int value, String comments) {
        final Rating rating=new Rating(Common.currentUser.getUsername(),roomId,String.valueOf(value),comments);

        ratingTbl.push()
                .setValue(rating)
                .addOnCompleteListener(new OnCompleteListener<Void>(){
                    @Override
                    public void onComplete(@NonNull Task<Void> task){
                        Toast.makeText(MemberRoomDetailActivity.this,"Thank you giving feedback ~ ",Toast.LENGTH_SHORT).show();

                    }

                });

    }
    private void getRatingRoom(String roomId) {
        com.google.firebase.database.Query roomRating=ratingTbl.orderByChild("roomId").equalTo(roomId);

        roomRating.addValueEventListener(new ValueEventListener() {
            int count=0,sum=0;
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot postSnapshot:dataSnapshot.getChildren()){

                    Rating item=postSnapshot.getValue(Rating.class);
                    sum+=Integer.parseInt(item.getRateValue());
                    count++;
                }
                if(count!=0) {
                    float average = sum / count;
                    ratingBar.setRating(average);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void getDetailRoom(String roomId) {
        rooms.child(roomId).addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot){
                currentRoom = dataSnapshot.getValue(Room.class);

                //Set Image
                Picasso.with(getBaseContext()).load(currentRoom.getImage()).into(room_image);

                collapsingToolbarLayout.setTitle(currentRoom.getName());

                room_price.setText("RM "+currentRoom.getPrice());

                room_name.setText(currentRoom.getName());

                room_description.setText(currentRoom.getDescription());

            }

            @Override
            public void onCancelled(DatabaseError databaseError){

            }
        });

    }
}

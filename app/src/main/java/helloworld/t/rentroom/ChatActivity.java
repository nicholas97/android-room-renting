package helloworld.t.rentroom;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import helloworld.t.rentroom.Adapter.ChatAdapter;
import helloworld.t.rentroom.Common.Common;
import helloworld.t.rentroom.Interface.ItemClickListener;
import helloworld.t.rentroom.Model.Chat;
import helloworld.t.rentroom.Model.Room;
import helloworld.t.rentroom.ViewHolder.ChatViewHolder;
import helloworld.t.rentroom.ViewHolder.RoomViewHolder;

public class ChatActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RelativeLayout ChatRootLayout;
    FirebaseDatabase db;
    DatabaseReference chatList;
    List<Chat> mchat;
    ChatAdapter chatAdapter;

    String appointmentId="";

    EditText edtMsg;
    ImageButton btn_send;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);


        db=FirebaseDatabase.getInstance();
        chatList=db.getReference("Chats");

        recyclerView=(RecyclerView)findViewById(R.id.recycle_view);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        ChatRootLayout=(RelativeLayout)findViewById(R.id.rootChat);
        edtMsg=(EditText)findViewById(R.id.text_send);

        btn_send=(ImageButton)findViewById(R.id.btn_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String msg=edtMsg.getText().toString();
                if(!msg.equals("")){

                    sendMessage(msg);

                }else{
                    Toast.makeText(ChatActivity.this, "You can't send empty message", Toast.LENGTH_SHORT).show();
                }
                edtMsg.setText("");
            }
        });

        if(getIntent() != null) {
            appointmentId = getIntent().getStringExtra(Common.appointmentId);
        }
        if(!appointmentId.isEmpty()){
            if(Common.isConnectedToInternet(getBaseContext())) {
                readMessages();
            }
            else{
                Toast.makeText(ChatActivity.this,"Please check your connection!",Toast.LENGTH_SHORT).show();
                return;


            }
        }


    }
    private void sendMessage(String msg){
        Chat newChat=new Chat();
        newChat.setAppointmentId(appointmentId);
        newChat.setIsOwner(Common.currentUser.getIsOwner());
        newChat.setMsg(msg);
        chatList.push().setValue(newChat);
    }

    private void readMessages(){
        mchat=new ArrayList<>();
        chatList.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mchat.clear();
                for(DataSnapshot snapshot:dataSnapshot.getChildren()){
                    Chat chat=snapshot.getValue(Chat.class);
                    if(chat.getAppointmentId().equals(appointmentId)){
                        mchat.add(chat);
                    }
                    chatAdapter= new ChatAdapter(ChatActivity.this,mchat);
                    recyclerView.setAdapter(chatAdapter);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}

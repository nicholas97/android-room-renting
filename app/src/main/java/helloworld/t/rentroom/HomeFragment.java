package helloworld.t.rentroom;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import helloworld.t.rentroom.Common.Common;

public class HomeFragment extends Fragment {

    private TextView username;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        username = view.findViewById(R.id.homeUsernameTextView);

        username.setText(Common.currentUser.getName()+"("+Common.getCurrentUserRole()+")");
        return view;

    }
}

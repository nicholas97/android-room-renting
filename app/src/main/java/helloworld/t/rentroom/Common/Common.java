package helloworld.t.rentroom.Common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import helloworld.t.rentroom.Model.User;

public class Common {
    public static User currentUser;

    public static final String appointmentId="appointmentId";
    public static final String roomId="roomId";
    public static final String UPDATE="Update";
    public static final String DELETE="Delete";
    public static final int PICK_IMAGE_REQUEST=71;

    public static String getCurrentUserRole(){
        if(currentUser.getIsOwner().equals("true")){
            return "Owner";
        }else{
            return "Member";
        }
    }


    public static boolean isConnectedToInternet(Context context){
        ConnectivityManager connectivityManager=(ConnectivityManager)context.getSystemService(context.CONNECTIVITY_SERVICE);
        if(connectivityManager!=null){
            NetworkInfo[] info=connectivityManager.getAllNetworkInfo();
            if(info!=null){
                for(int i=0;i<info.length;i++){
                    if(info[i].getState()==NetworkInfo.State.CONNECTED){
                        return true;

                    }

                }

            }

        }return false;

    }
}

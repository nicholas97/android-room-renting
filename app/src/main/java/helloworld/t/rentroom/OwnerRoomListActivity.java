package helloworld.t.rentroom;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.security.acl.Owner;
import java.util.UUID;

import helloworld.t.rentroom.Common.Common;
import helloworld.t.rentroom.Interface.ItemClickListener;
import helloworld.t.rentroom.Model.Room;
import helloworld.t.rentroom.ViewHolder.RoomViewHolder;

public class OwnerRoomListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    RelativeLayout ownerRoomRootLayout;

    FloatingActionButton fab;

    //Firebase
    FirebaseDatabase db;
    DatabaseReference roomList;
    FirebaseStorage storage;
    StorageReference storageReference;
    FirebaseRecyclerAdapter<Room, RoomViewHolder> adapter;

    EditText edtName,edtDescription,edtPrice,edtStatus;
    Button btnSelect,btnUpload;

    Room newRoom;
    Uri saveUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_room_list);

        db=FirebaseDatabase.getInstance();
        roomList=db.getReference("Rooms");
        storage=FirebaseStorage.getInstance();
        storageReference=storage.getReference();

        //Init
        recyclerView=(RecyclerView)findViewById(R.id.recycler_owner_room);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        ownerRoomRootLayout=(RelativeLayout)findViewById(R.id.ownerRoomRootLayout);

        fab=(FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddRoomDialog();
            }
        });
        loadListRoom();

    }


    private void loadListRoom() {

        Query listRoomByBuildingId=roomList.orderByChild("ownerUsername").equalTo(Common.currentUser.getUsername());

        FirebaseRecyclerOptions<Room> options=new FirebaseRecyclerOptions.Builder<Room>()
                .setQuery(listRoomByBuildingId,Room.class)
                .build();
        adapter=new FirebaseRecyclerAdapter<Room, RoomViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull RoomViewHolder viewHolder, int position, @NonNull Room model) {
                viewHolder.txtRoomName.setText(model.getName());
                viewHolder.txtRoomStatus.setText(model.getStatus());
                Picasso.with(getBaseContext()).load(model.getImage()).into(viewHolder.imageView);

                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent roomDetail=new Intent(OwnerRoomListActivity.this,OwnerRoomDetailActivity.class);
                        roomDetail.putExtra(Common.roomId,adapter.getRef(position).getKey());
                        startActivity(roomDetail);


                    }
                });
            }

            @NonNull
            @Override
            public RoomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View itemView=LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.room_item,viewGroup,false);
                return new RoomViewHolder(itemView);
            }
        };

        adapter.startListening();
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    private void showAddRoomDialog() {

        AlertDialog.Builder alertDialog=new AlertDialog.Builder(OwnerRoomListActivity.this);
        alertDialog.setTitle("Add new Room");
        alertDialog.setMessage("Please fill full information");

        LayoutInflater inflater=this.getLayoutInflater();
        View add_room_layout=inflater.inflate(R.layout.add_new_room,null);

        edtName=add_room_layout.findViewById(R.id.edtRoomName);
        edtDescription=add_room_layout.findViewById(R.id.edtRoomDescription);
        edtPrice=add_room_layout.findViewById(R.id.edtRoomPrice);
        edtStatus=add_room_layout.findViewById(R.id.edtRoomStatus);


        btnSelect=add_room_layout.findViewById(R.id.btnSelect);
        btnUpload=add_room_layout.findViewById(R.id.btnUpload);

        //Event for button
        btnSelect.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                chooseImage();//user selectimage from galeery and save uri of the image

            }

        });
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImage();
            }
        });



        alertDialog.setView(add_room_layout);
        alertDialog.setIcon(R.drawable.ic_add_circle_black_24dp);

        //Set Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

                if(newRoom!=null){
                    //roomList.child(edtId.getText().toString()).setValue(newRoom);
                    roomList.push().setValue(newRoom);
                    Snackbar.make(ownerRoomRootLayout,"New Room "+newRoom.getName()+"was added",Snackbar.LENGTH_SHORT).show();

                }

            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }

    private void uploadImage() {
        if(saveUri!=null){
            final ProgressDialog mDialog=new ProgressDialog(this);
            mDialog.setMessage("Uploading...");
            mDialog.show();

            String imageName= UUID.randomUUID().toString();
            final StorageReference imageFolder=storageReference.child("images/"+imageName);
            imageFolder.putFile(saveUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            mDialog.dismiss();
                            Toast.makeText(OwnerRoomListActivity.this,"Uploaded !!!",Toast.LENGTH_SHORT).show();
                            imageFolder.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {

                                    newRoom=new Room();
                                    newRoom.setName(edtName.getText().toString());
                                    newRoom.setDescription(edtDescription.getText().toString());
                                    newRoom.setPrice(edtPrice.getText().toString());
                                    newRoom.setStatus(edtStatus.getText().toString());
                                    newRoom.setOwnerUsername(Common.currentUser.getUsername());
                                    newRoom.setImage(uri.toString());

                                }
                            });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            mDialog.dismiss();
                            Toast.makeText(OwnerRoomListActivity.this,""+e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            double progress=(100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                            mDialog.setMessage("Uploaded"+progress+"%");
                        }
                    });

        }
    }

    private void chooseImage(){
        Intent intent=new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), Common.PICK_IMAGE_REQUEST);

    }

    //

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==Common.PICK_IMAGE_REQUEST&&resultCode==RESULT_OK&&data!=null&&data.getData()!=null){

            saveUri=data.getData();
            btnSelect.setText("Image Selected !");
        }
    }

    private void deleteRoom(String key) {

        roomList.child(key).removeValue();
        Snackbar.make(ownerRoomRootLayout,"Room was deleted",Snackbar.LENGTH_SHORT).show();
    }

    private void showUpdateRoomDialog(final String key, final Room item) {
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(OwnerRoomListActivity.this);
        alertDialog.setTitle("Edit Room");
        alertDialog.setMessage("Please fill full information");

        LayoutInflater inflater=this.getLayoutInflater();
        View add_room_layout=inflater.inflate(R.layout.add_new_room,null);

        edtName=add_room_layout.findViewById(R.id.edtRoomName);
        edtDescription=add_room_layout.findViewById(R.id.edtRoomDescription);
        edtPrice=add_room_layout.findViewById(R.id.edtRoomPrice);
        edtStatus=add_room_layout.findViewById(R.id.edtRoomStatus);


        edtName.setText(item.getName());
        edtDescription.setText(item.getDescription());
        edtPrice.setText(item.getPrice());
        edtStatus.setText(item.getStatus());


        btnSelect=add_room_layout.findViewById(R.id.btnSelect);
        btnUpload=add_room_layout.findViewById(R.id.btnUpload);

        //Event for button
        btnSelect.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                chooseImage();//user selectimage from galeery and save uri of the image

            }

        });
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeImage(item);
            }
        });



        alertDialog.setView(add_room_layout);
        alertDialog.setIcon(R.drawable.ic_room);

        //Set Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

                item.setName(edtName.getText().toString());
                item.setPrice(edtPrice.getText().toString());
                item.setDescription(edtDescription.getText().toString());
                item.setStatus(edtStatus.getText().toString());

                roomList.child(key).setValue(item);


                Snackbar.make(ownerRoomRootLayout,"Room "+item.getName()+"was edited",Snackbar.LENGTH_SHORT).show();



            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }
    private void changeImage(final Room item) {
        if(saveUri!=null){
            final ProgressDialog mDialog=new ProgressDialog(this);
            mDialog.setMessage("Uploading...");
            mDialog.show();

            String imageName=UUID.randomUUID().toString();
            final StorageReference imageFolder=storageReference.child("images/"+imageName);
            imageFolder.putFile(saveUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            mDialog.dismiss();
                            Toast.makeText(OwnerRoomListActivity.this,"Uploaded !!!",Toast.LENGTH_SHORT).show();
                            imageFolder.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    //set value for new Category if image upload and we can get download link
                                    item.setImage(uri.toString());
                                }
                            });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            mDialog.dismiss();
                            Toast.makeText(OwnerRoomListActivity.this,""+e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                            double progress=(100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                            mDialog.setMessage("Uploaded"+progress+"%");
                        }
                    });

        }
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getTitle().equals(Common.UPDATE))
        {
            showUpdateRoomDialog(adapter.getRef(item.getOrder()).getKey(),adapter.getItem(item.getOrder()));

        }else if(item.getTitle().equals(Common.DELETE))
        {

            deleteRoom(adapter.getRef(item.getOrder()).getKey());
        }
        return super.onContextItemSelected(item);
    }

}

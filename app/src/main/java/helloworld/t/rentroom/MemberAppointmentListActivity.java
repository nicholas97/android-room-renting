package helloworld.t.rentroom;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import helloworld.t.rentroom.Common.Common;
import helloworld.t.rentroom.Model.Appointment;
import helloworld.t.rentroom.ViewHolder.AppointmentViewHolder;

public class MemberAppointmentListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseRecyclerAdapter<Appointment, AppointmentViewHolder> adapter;


    FirebaseDatabase database;
    DatabaseReference appointments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_appointment_list);

        database = FirebaseDatabase.getInstance();
        appointments = database.getReference("Appointments");

        recyclerView = findViewById(R.id.listAppointments);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        loadAppointments(Common.currentUser.getUsername());
    }


    private void loadAppointments(String username) {

        Query getAppointmentByUser=appointments.orderByChild("memberUsername").equalTo(username);

        FirebaseRecyclerOptions<Appointment> orderOptions=new FirebaseRecyclerOptions.Builder<Appointment>()
                .setQuery(getAppointmentByUser,Appointment.class)
                .build();

        adapter=new FirebaseRecyclerAdapter<Appointment, AppointmentViewHolder>(orderOptions) {

            @Override
            protected void onBindViewHolder(@NonNull AppointmentViewHolder holder, final int position, @NonNull final Appointment model) {

                holder.txtAppointmentId.setText("Id : "+adapter.getRef(position).getKey());
                holder.txtOwnerUsername.setText("Owner Username : "+model.getOwnerUsername());
                holder.txtRoomId.setText("Room Id : "+model.getRoomId());
                holder.txtStatus.setText("Status : "+model.getStatus());
                holder.txtDateTime.setText("Date/Time : "+model.getDateTime());
                holder.btn_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(adapter.getItem(position).getStatus().equals("Requesting"))
                            deleteAppointment(adapter.getRef(position).getKey());
                        else
                            Toast.makeText(MemberAppointmentListActivity.this, "Can't Delete this Appointment !", Toast.LENGTH_SHORT).show();
                    }
                });

                holder.btn_chat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent chat=new Intent(MemberAppointmentListActivity.this,ChatActivity.class);
                        chat.putExtra(Common.appointmentId,adapter.getRef(position).getKey());
                        startActivity(chat);


                    }
                });
            }


            @Override
            public AppointmentViewHolder onCreateViewHolder(ViewGroup parent, int i) {
                View itemView= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.appointment_item,parent,false);
                return new AppointmentViewHolder(itemView);
            }
        };
        adapter.startListening();
        recyclerView.setAdapter(adapter);

    }
    private void deleteAppointment(final String key) {
        appointments.child(key)
                .removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(MemberAppointmentListActivity.this, new StringBuilder("Appointment")
                        .append(key)
                        .append("has been deleted!").toString(), Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(MemberAppointmentListActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}

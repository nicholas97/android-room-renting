package helloworld.t.rentroom;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import helloworld.t.rentroom.Common.Common;
import helloworld.t.rentroom.Model.User;

public class RegisterActivity extends AppCompatActivity {

    MaterialEditText edtUsername, edtName, edtPassword;
    Button btnRegister;
    Boolean isOwner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        edtUsername=findViewById(R.id.edtUsername);
        edtName=findViewById(R.id.edtName);
        edtPassword=findViewById(R.id.edtPassword);

        btnRegister=findViewById(R.id.btnRegister);


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_user = database.getReference("User");

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Common.isConnectedToInternet(getBaseContext())){
                    final ProgressDialog mDialog = new ProgressDialog(RegisterActivity.this);
                    mDialog.setMessage("Please Wating...");
                    mDialog.show();

                    table_user.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //check if user not exist in database
                            if (dataSnapshot.child(edtUsername.getText().toString()).exists()) {
                                //get user information
                                mDialog.dismiss();
                                User user = dataSnapshot.child(edtUsername.getText().toString()).getValue(User.class);
                                Toast.makeText(RegisterActivity.this, "Username already registered !", Toast.LENGTH_SHORT).show();

                            } else {
                                mDialog.dismiss();
                                User user = new User(edtName.getText().toString(), edtPassword.getText().toString());
                                user.setIsOwner(isOwner.toString());
                                table_user.child(edtUsername.getText().toString()).setValue(user);
                                Toast.makeText(RegisterActivity.this, "Register Successfully !", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }
                        @Override
                        public void onCancelled (DatabaseError databaseError){

                        }
                    });
                }else{
                    Toast.makeText(RegisterActivity.this,"Please check your connection!",Toast.LENGTH_SHORT).show();
                    return;

                }
            }
        });

        if(getIntent() != null) {
            isOwner = getIntent().getBooleanExtra("isOwner", false);
        }
    }
}

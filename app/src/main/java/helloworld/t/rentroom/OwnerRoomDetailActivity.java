package helloworld.t.rentroom;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.stepstone.apprating.listener.RatingDialogListener;

import helloworld.t.rentroom.Common.Common;
import helloworld.t.rentroom.Model.Rating;
import helloworld.t.rentroom.Model.Room;

public class OwnerRoomDetailActivity extends AppCompatActivity{

    TextView room_name,room_price,room_description;
    ImageView room_image;
    CollapsingToolbarLayout collapsingToolbarLayout;
    RatingBar ratingBar;

    FirebaseDatabase database;
    DatabaseReference rooms;
    DatabaseReference ratingTbl;

    Button btnShowComment;

    String roomId = "";

    Room currentRoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_room_detail);

        database = FirebaseDatabase.getInstance();
        rooms = database.getReference("Rooms");
        ratingTbl=database.getReference("Rating");

        ratingBar=(RatingBar)findViewById(R.id.ratingBar);
        room_description = (TextView) findViewById(R.id.room_description);
        room_name = (TextView) findViewById(R.id.room_name);
        room_price = (TextView) findViewById(R.id.room_price);
        room_image = (ImageView) findViewById(R.id.img_room);

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppbar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppbar);





        btnShowComment=(Button)findViewById(R.id.btnShowComment);
        btnShowComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(OwnerRoomDetailActivity.this,ShowCommentActivity.class);
                intent.putExtra(Common.roomId,roomId);
                startActivity(intent);


            }
        });

        if(getIntent() != null) {
            roomId = getIntent().getStringExtra(Common.roomId);
        }
        if(!roomId.isEmpty()){
            if(Common.isConnectedToInternet(getBaseContext())) {
                getDetailRoom(roomId);
                getRatingRoom(roomId);
            }
            else{
                Toast.makeText(OwnerRoomDetailActivity.this,"Please check your connection!",Toast.LENGTH_SHORT).show();
                return;


            }
        }


    }

    private void getRatingRoom(String roomId) {
        com.google.firebase.database.Query roomRating=ratingTbl.orderByChild("roomId").equalTo(roomId);

        roomRating.addValueEventListener(new ValueEventListener() {
            int count=0,sum=0;
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot postSnapshot:dataSnapshot.getChildren()){

                    Rating item=postSnapshot.getValue(Rating.class);
                    sum+=Integer.parseInt(item.getRateValue());
                    count++;
                }
                if(count!=0) {
                    float average = sum / count;
                    ratingBar.setRating(average);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void getDetailRoom(String roomId) {
        rooms.child(roomId).addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot){
                currentRoom = dataSnapshot.getValue(Room.class);

                //Set Image
                Picasso.with(getBaseContext()).load(currentRoom.getImage()).into(room_image);

                collapsingToolbarLayout.setTitle(currentRoom.getName());

                room_price.setText("RM "+currentRoom.getPrice());

                room_name.setText(currentRoom.getName());

                room_description.setText(currentRoom.getDescription());

            }

            @Override
            public void onCancelled(DatabaseError databaseError){

            }
        });

    }
}

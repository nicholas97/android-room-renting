package helloworld.t.rentroom.Model;

public class Chat {
    private String Msg,AppointmentId,isOwner;

    public Chat() {
    }

    public Chat(String msg, String appointmentId, String isOwner) {
        Msg = msg;
        AppointmentId = appointmentId;
        this.isOwner = isOwner;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String msg) {
        Msg = msg;
    }

    public String getAppointmentId() {
        return AppointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        AppointmentId = appointmentId;
    }

    public String getIsOwner() {
        return isOwner;
    }

    public void setIsOwner(String isOwner) {
        this.isOwner = isOwner;
    }
}

package helloworld.t.rentroom.Model;

public class Room {
    private String Name,Image,Description,Price,Status,OwnerUsername;

    public Room() {
    }

    public Room(String name, String image, String description, String price, String status, String ownerUsername) {
        Name = name;
        Image = image;
        Description = description;
        Price = price;
        Status = status;
        OwnerUsername = ownerUsername;
    }

    public String getOwnerUsername() {
        return OwnerUsername;
    }

    public void setOwnerUsername(String ownerUsername) {
        OwnerUsername = ownerUsername;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}

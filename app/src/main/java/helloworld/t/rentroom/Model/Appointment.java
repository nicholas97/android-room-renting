package helloworld.t.rentroom.Model;

public class Appointment {
    private String MemberUsername,OwnerUsername,RoomId,DateTime,Status;

    public Appointment() {
    }

    public Appointment(String memberUsername, String ownerUsername, String roomId, String dateTime, String status) {
        MemberUsername = memberUsername;
        OwnerUsername = ownerUsername;
        RoomId = roomId;
        DateTime = dateTime;
        Status = "Requesting";
    }

    public String getMemberUsername() {
        return MemberUsername;
    }

    public void setMemberUsername(String memberUsername) {
        MemberUsername = memberUsername;
    }

    public String getOwnerUsername() {
        return OwnerUsername;
    }

    public void setOwnerUsername(String ownerUsername) {
        OwnerUsername = ownerUsername;
    }

    public String getRoomId() {
        return RoomId;
    }

    public void setRoomId(String roomId) {
        RoomId = roomId;
    }

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}

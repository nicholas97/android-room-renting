package helloworld.t.rentroom.Model;

public class Rating {


    private String username;
    private String roomId;
    private String rateValue;
    private String comment;

    public Rating() {
    }

    public Rating(String username, String roodId, String rateValue, String comment) {
        this.username = username;
        this.roomId = roodId;
        this.rateValue = rateValue;
        this.comment = comment;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRateValue() {
        return rateValue;
    }

    public void setRateValue(String rateValue) {
        this.rateValue = rateValue;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}

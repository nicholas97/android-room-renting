package helloworld.t.rentroom.Model;

public class User {
    private String Name;
    private String Password;
    private String Username;
    private String IsOwner;

    public User() {
    }

    public User(String name, String password) {
        Name = name;
        Password = password;
        IsOwner="false";
    }

    public String getIsOwner() {
        return IsOwner;
    }

    public void setIsOwner(String isStaff) {
        IsOwner = isStaff;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}


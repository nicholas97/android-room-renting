package helloworld.t.rentroom;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class RegisterRoleActivity extends AppCompatActivity {

    Button btnRegisterMember,btnRegisterOwner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_role);


        btnRegisterMember=findViewById(R.id.btnRegisterMember);
        btnRegisterMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent register =  new Intent(RegisterRoleActivity.this, RegisterActivity.class);
                register.putExtra("isOwner", false);
                startActivity(register);
            }
        });


        btnRegisterOwner=findViewById(R.id.btnRegisterOwner);
        btnRegisterOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent register =  new Intent(RegisterRoleActivity.this, RegisterActivity.class);
                register.putExtra("isOwner", true);
                startActivity(register);
                finish();
            }
        });
    }
}

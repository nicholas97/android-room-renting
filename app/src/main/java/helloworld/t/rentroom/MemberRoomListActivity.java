package helloworld.t.rentroom;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import helloworld.t.rentroom.Common.Common;
import helloworld.t.rentroom.Interface.ItemClickListener;
import helloworld.t.rentroom.Model.Room;
import helloworld.t.rentroom.ViewHolder.RoomViewHolder;

public class MemberRoomListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    RelativeLayout memberRoomRootLayout;

    //Firebase
    FirebaseDatabase db;
    DatabaseReference roomList;
    FirebaseStorage storage;
    StorageReference storageReference;
    FirebaseRecyclerAdapter<Room, RoomViewHolder> adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_room_list);


        db=FirebaseDatabase.getInstance();
        roomList=db.getReference("Rooms");
        storage=FirebaseStorage.getInstance();
        storageReference=storage.getReference();


        //Init
        recyclerView=(RecyclerView)findViewById(R.id.recycler_member_room);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        memberRoomRootLayout=(RelativeLayout)findViewById(R.id.memberRoomRootLayout);

        loadListRoom();
    }

    private void loadListRoom() {

        Query listRoomByBuildingId=roomList.orderByChild("status").equalTo("Available");

        FirebaseRecyclerOptions<Room> options=new FirebaseRecyclerOptions.Builder<Room>()
                .setQuery(listRoomByBuildingId,Room.class)
                .build();
        adapter=new FirebaseRecyclerAdapter<Room, RoomViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull RoomViewHolder viewHolder, int position, @NonNull Room model) {
                viewHolder.txtRoomName.setText(model.getName());
                viewHolder.txtRoomStatus.setVisibility(View.INVISIBLE);
                Picasso.with(getBaseContext()).load(model.getImage()).into(viewHolder.imageView);

                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent roomDetail=new Intent(MemberRoomListActivity.this,MemberRoomDetailActivity.class);
                        roomDetail.putExtra(Common.roomId,adapter.getRef(position).getKey());
                        startActivity(roomDetail);




                    }
                });
            }

            @NonNull
            @Override
            public RoomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View itemView= LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.room_item,viewGroup,false);
                return new RoomViewHolder(itemView);
            }
        };

        adapter.startListening();
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

}

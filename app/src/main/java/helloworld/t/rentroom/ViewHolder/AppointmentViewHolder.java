package helloworld.t.rentroom.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import helloworld.t.rentroom.Interface.ItemClickListener;
import helloworld.t.rentroom.R;

public class AppointmentViewHolder extends RecyclerView.ViewHolder {
    public TextView txtOwnerUsername,txtDateTime,txtRoomId,txtStatus,txtAppointmentId;
    private ItemClickListener itemClickListener;
    public  ImageView btn_delete,btn_chat;

    public AppointmentViewHolder(View itemView){
        super(itemView);
        txtAppointmentId=itemView.findViewById(R.id.txtAppointmentId);
        txtOwnerUsername=itemView.findViewById(R.id.txtOwnerUsername);
        txtDateTime=itemView.findViewById(R.id.txtDateTime);
        txtRoomId=itemView.findViewById(R.id.txtRoomId);
        txtStatus=itemView.findViewById(R.id.txtStatus);
        btn_delete=itemView.findViewById(R.id.btn_delete);
        btn_chat=itemView.findViewById(R.id.btn_chat);
    }
}

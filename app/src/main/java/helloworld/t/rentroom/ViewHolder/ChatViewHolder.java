package helloworld.t.rentroom.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import helloworld.t.rentroom.Interface.ItemClickListener;
import helloworld.t.rentroom.R;

public class ChatViewHolder extends RecyclerView.ViewHolder {
    public TextView show_message;

    public ChatViewHolder(View itemView){
        super(itemView);
        show_message=itemView.findViewById(R.id.show_message);
    }
}

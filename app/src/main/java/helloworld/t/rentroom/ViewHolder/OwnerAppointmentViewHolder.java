package helloworld.t.rentroom.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import helloworld.t.rentroom.Interface.ItemClickListener;
import helloworld.t.rentroom.R;

public class OwnerAppointmentViewHolder extends RecyclerView.ViewHolder {
    public TextView txtMemberUsername,txtDateTime,txtRoomId,txtStatus,txtAppointmentId;
    private ItemClickListener itemClickListener;
    public ImageView btn_cancel,btn_deal,btn_chat;

    public OwnerAppointmentViewHolder(View itemView){
        super(itemView);
        txtAppointmentId=itemView.findViewById(R.id.txtAppointmentId);
        txtMemberUsername=itemView.findViewById(R.id.txtMemberUsername);
        txtDateTime=itemView.findViewById(R.id.txtDateTime);
        txtRoomId=itemView.findViewById(R.id.txtRoomId);
        txtStatus=itemView.findViewById(R.id.txtStatus);
        btn_cancel=itemView.findViewById(R.id.btn_cancel);
        btn_deal=itemView.findViewById(R.id.btn_deal);
        btn_chat=itemView.findViewById(R.id.btn_chat);
    }
}

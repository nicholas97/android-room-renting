package helloworld.t.rentroom.ViewHolder;

import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import helloworld.t.rentroom.R;

public class ShowCommentViewHolder extends RecyclerView.ViewHolder{


    public TextView txtUsername,txtComment;
    public RatingBar ratingBar;

    public ShowCommentViewHolder(View itemView) {
        super(itemView);

        txtComment=(TextView)itemView.findViewById(R.id.txtComment);
        txtUsername=(TextView)itemView.findViewById(R.id.txtUsername);
        ratingBar=(RatingBar) itemView.findViewById(R.id.ratingBar);



    }

}
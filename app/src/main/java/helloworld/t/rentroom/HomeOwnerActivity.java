package helloworld.t.rentroom;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import helloworld.t.rentroom.Common.Common;

public class HomeOwnerActivity extends AppCompatActivity {


    private BottomNavigationView bottomNav;
    private Fragment selectedFragment = new HomeFragment();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_owner);


        bottomNav = findViewById(R.id.bottom_nav);

        bottomNav.setOnNavigationItemSelectedListener(navListener);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectedFragment).commit();
    }
    private BottomNavigationView.OnNavigationItemSelectedListener navListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {


            switch (menuItem.getItemId()){
                case R.id.navigation_home:
                    selectedFragment = new HomeFragment();
                    break;
                case R.id.navigation_room:
                    Intent roomList=new Intent(HomeOwnerActivity.this,OwnerRoomListActivity.class);
                    startActivity(roomList);
                    break;
                case R.id.navigation_appointment:
                    Intent appointmentList=new Intent(HomeOwnerActivity.this,OwnerAppointmentListActivity.class);
                    startActivity(appointmentList);
                    break;
                case R.id.navigation_logout:
                    Common.currentUser=null;
                    Intent login = new Intent(HomeOwnerActivity.this, LoginActivity.class);
                    login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(login);
                    finish();
                    break;
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectedFragment).commit();

            return true;
        }
    };
}
